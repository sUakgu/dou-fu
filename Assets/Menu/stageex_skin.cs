﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stageex_skin : MonoBehaviour {
    SpriteRenderer MainSpriteRenderer;
    public Sprite[] skin;

    // Use this for initialization
    void Start ()
    {
        MainSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        gameObject.SetActive(false);        //最初にボタンを消す
        if (picbutton.clearst >= 4)         //stage4をクリアしているとボタンを表示する。
        {
            gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (picbutton.buttonch == 5)      //ボタンが押されるとスキンを変更する
        {
            MainSpriteRenderer.sprite = skin[1];
        }
    }
}
