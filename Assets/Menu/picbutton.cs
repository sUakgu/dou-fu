﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class picbutton : MonoBehaviour {
    public static int buttonch,clearst;

    // Use this for initialization
    private void Awake()        //ゲームの起動後一番に読み込み
    {
        if (!PlayerPrefs.HasKey("CLEARSTAGE"))      //もしCLEARSTAGEが空なら、0を代入する
        {
            PlayerPrefs.GetInt("CLEARSTAGE", 0);
        }
        clearst = PlayerPrefs.GetInt("CLEARSTAGE");     //clearst(このスクリプト限定のクリア判定関数)にCLAERSTAGEを代入
    }

    void Start () {
       
        
        buttonch = 0;     //グローバル変数chの初期化
        
	}
	
	// Update is called once per frame
	void Update () {
       
        if (Input.GetMouseButtonDown(0))        //マウスのカーソル位置読み込み(今後スマホ向けにするにあたって改良必須)
        {
           
            Ray ray = new Ray();
            RaycastHit hit = new RaycastHit();
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            
            if (Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity))        //それぞれのボタンをクリックしたときの反応(0.3秒後に下の対応する関数を呼び出し)
            {
                if (hit.collider.gameObject.CompareTag("tu-toriaru"))
                {
                    buttonch = 10;
                    Invoke("stage0", 0.3f);
                }
                if(hit.collider.gameObject.CompareTag("Stage1"))
                {
                    buttonch=1;
                    Invoke("stage1",0.3f);
                   
                }
                if (hit.collider.gameObject.CompareTag("Stage2"))
                {
                    buttonch = 2;
                    Invoke("stage2", 0.3f);

                }
                if (hit.collider.gameObject.CompareTag("Stage3"))
                {
                    buttonch = 3;
                    Invoke("stage3", 0.3f);

                }
                if (hit.collider.gameObject.CompareTag("Stage4"))
                {
                    buttonch = 4;
                    Invoke("stage4", 0.3f);

                }
                if (hit.collider.gameObject.CompareTag("Config"))
                {
                    
                    Invoke("Config", 0.3f);
                }
                if (hit.collider.gameObject.CompareTag("StageEX"))
                {
                    buttonch = 5;
                    Invoke("stageEX", 0.3f);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Space))        //debag用コマンド(スペースキーでステージのクリア履歴を消去)
        {
            PlayerPrefs.DeleteKey("CLEARSTAGE");
        }
    }
    void stage0()       //ボタンクリック時に呼び出される関数群、シーン遷移のみ
    {
        SceneManager.LoadScene("Stage0");
    }
    void stage1()
    {
        SceneManager.LoadScene("Stage1");
    }
    void stage2()
    {
        SceneManager.LoadScene("Stage2");
    }
    void stage3()
    {
        SceneManager.LoadScene("Stage3");
    }
    void stage4()
    {
        SceneManager.LoadScene("Stage4");
    }
    void Config()
    {
        SceneManager.LoadScene("Config");
    }
    void stageEX()
    {
        SceneManager.LoadScene("stageEX");
    }

}
