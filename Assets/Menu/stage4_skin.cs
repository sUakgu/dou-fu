﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stage4_skin : MonoBehaviour {
    SpriteRenderer MainSpriteRenderer;
    public Sprite[] skin;

    void Start () {
        MainSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        gameObject.SetActive(false);        //最初にボタンを消す
        if (picbutton.clearst >= 3)         //stage3をクリアしているとボタンを表示
        {
            gameObject.SetActive(true);
        }
    }
	
	// Update is called once per frame
	void Update () {
        
        if (picbutton.buttonch == 4)
        {
            MainSpriteRenderer.sprite = skin[1];
        }
    }
}
