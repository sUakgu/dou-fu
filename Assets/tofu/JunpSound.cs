﻿using UnityEngine;
using System.Collections;
 
public class JunpSound : MonoBehaviour
{

    public AudioSource sound01;
    public AudioSource sound02;

    variables Val;

    void Start()
    {
        //AudioSourceコンポーネントを取得し、変数に格納
        Val = GameObject.Find("Main Camera").GetComponent<variables>();
        AudioSource[] audioSources = GetComponents<AudioSource>();
        sound01 = audioSources[0];
        sound02 = audioSources[1];
    }

    void Update()
    {
        //指定のキーが押されたら音声ファイル再生
        if (Input.GetKeyDown(KeyCode.UpArrow) && TofuPlayer.soundcheck == 1)
        {
            sound01.PlayOneShot(sound01.clip);
        }
        if (Val.landcheck == true)
        {
            sound02.PlayOneShot(sound02.clip);
        }
    }
}