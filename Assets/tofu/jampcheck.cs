﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jampcheck : MonoBehaviour
{
    
    variables Val;
    BoxCollider2D col;
    float tofuwidth, tofuheight;//豆腐の横幅と高さ
    float thiswidth;
    float objsize = 0.6f;//接地判定オブジェクトの横幅と豆腐の横幅の比
    Vector2 tofupos;
                         // Use this for initialization
    void Start()
    {
       
        col = this.gameObject.GetComponent<BoxCollider2D>();
        Val = GameObject.Find("Main Camera").GetComponent<variables>();
    }

    // Update is called once perthis frame
    void Update()
    {
        tofuwidth = Val.tofuwidth; //豆腐の幅を取得
        tofuheight = Val.tofuheight;   //豆腐の高さを取得
        tofupos = Val.tofupos;  //豆腐の座標を取得
        thiswidth = tofuwidth * objsize;
        col.size = new Vector2(thiswidth, tofuheight * 0.15f);//接地判定の幅を設定、高さを豆腐の高さの10%にする
        this.gameObject.transform.position = new Vector2(tofupos.x, tofupos.y - tofuheight / 2);//接地判定の座標を豆腐の座標と豆腐の高さを参考にした座標にする
       

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Val.landcheck = true;//床に触れたときlandCheckを真にする

    }
    private void OnTriggerExit2D(Collider2D other)
    {
        Val.jumpcheck = true;//床から離れたときjumpCheckを真にする
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        Val.jumpcheck = false;
    }

}
