﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class variables : MonoBehaviour {
    TofuPlayer tofumain;
    public Vector2 tofupos;
    public float tofuwidth, tofuheight;
    public bool jumpcheck,beanscheck;
    public int getkey;
    public int Bact;
    public bool landcheck;
    

    // Use this for initialization
    void Start () {
        tofumain = GameObject.Find("TofuPlayer").GetComponent<TofuPlayer>();
        jumpcheck = false;
        beanscheck = false;
        landcheck = false;
        getkey = 0;
        Bact = 0;
        

	}
	
	// Update is called once per frame
	void Update () {
        tofupos = tofumain.thispos;
        tofuwidth = tofumain.width;
        tofuheight = tofumain.height;
	}
}
