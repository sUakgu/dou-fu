﻿using UnityEngine;
using System.Collections;

public class TofuPlayer : MonoBehaviour
{
    variables Val;

    [SerializeField]
    private float walkspeed;
    [SerializeField]
    private float jumpPowerB;//ジャンプのベースの力
    [SerializeField]
    private float MidjumpP;
    [SerializeField]
    private float MinjumpP;
    [SerializeField]
    private float jumpingSpeed;
    


    public Vector2 thispos;
    public float width, height;
    public Sprite[] junp;
    public Sprite[] walk; //プレイヤーの歩くスプライト配列
    public Sprite[] TofuShape; //コスチューム配列
    public GameObject[] TofuPiece; //豆腐のかけら
    public static int cut = 0; //豆腐のコスチューム
    public static int soundcheck= 0;

    //public Sprite[] junp;

    int animIndex; //歩くアニメーションのインデックス
    //int animIndex; //歩くアニメーションのインデックス
    float jumpPower;//実際にジャンプするときの力

    bool walkCheck; //歩いているかのチェック
    bool jumpCheck;

    float s = 0.0f;
    Vector2 tofudefScale = Vector2.zero;
    BoxCollider2D Tcollider; //豆腐のコライダー
 
    void Start()
    {
        //animIndex = 0;
        jumpPower = jumpPowerB;
        Val = GameObject.Find("Main Camera").GetComponent<variables>();
        walkCheck = false;
        tofudefScale = transform.lossyScale;//豆腐の最初のScaleを取得


    }

    void Update()
    {
        jumpCheck = Val.jumpcheck;
        thispos = gameObject.transform.position;
        width = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        height = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        Val.landcheck = false;

        //Scaleの設定
        Vector2 lossScale = transform.lossyScale;
        Vector2 localScale = transform.localScale;
       
        //ジャンプ力の設定
        if (cut == 1 || cut == 2)
        {
            jumpPower = jumpPowerB * MidjumpP;
        }
        else if (cut == 3)
        {
            jumpPower = jumpPowerB * MinjumpP;
        }
        else
        {
            jumpPower = jumpPowerB;
        }


        //キャラクタの基本動作
        if (jumpCheck)
        {
            // animIndex = 4;
        }
        float x = Input.GetAxis("Horizontal");//xに横→の値を代入
        if (jumpCheck)
        {
            s = walkspeed * jumpingSpeed;
        }
        else
        {
            s = walkspeed;
        }
        if (Time.timeScale == 0)
        {
            s = walkspeed * 0;
            //  animIndex = 0;
        }
        transform.Translate(x * s, 0, 0);//豆腐横移動
        if (x == 0.0f)
        {
            walkCheck = false;
        }
        else
        {
            walkCheck = true;
            transform.localScale = new Vector2((-1)*Mathf.Sign(x)*(localScale.x / lossScale.x * tofudefScale.x), localScale.y / lossScale.y * tofudefScale.y);
            /*↑この行のsign(x)にトウフの横の長さを乗算すれば大きさを変えずに反転させることができる*/
        }


        //ジャンプしていなくてなおかつジャンプしようとしたときに音を鳴らす
        if (Input.GetKeyDown(KeyCode.UpArrow) && jumpCheck == false)
        {
            if (soundcheck == 0)
            {
                soundcheck = 1;
            }
        }
        if(soundcheck == 1 && jumpCheck == true)
        {
            soundcheck = 0;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && !jumpCheck)//↑が押されたとき、ジャンプ中でないなら
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpPower);//ジャンプする
            jumpCheck = true;
        }


        
        //豆取得の関数
        GetBeans();   


        //とうふを切るスクリプト 中の細かい処理はいずれ関数にまとめて見やすくします
        Transform TF = this.transform;//とうふ座標取得
        Vector3 myTF = TF.position;
        if (Input.GetKeyDown(KeyCode.C) && cut == 0)
        {//縦切り
            cut = 1;
            GetComponent<SpriteRenderer>().sprite = TofuShape[cut];//コスチューム変更
            Tcollider = GetComponent<BoxCollider2D>();
            Instantiate(TofuPiece[cut - 1], new Vector3(myTF.x + 3, myTF.y, myTF.z), Quaternion.identity);//ピース生成
            Tcollider.size = new Vector2((float)1.5, (float)3.3);//当たり判定変更
            Debug.Log(Mathf.Sign(x));
        }
        else if (Input.GetKeyDown(KeyCode.V) && cut == 0)
        {//横切り
            cut = 2;
            GetComponent<SpriteRenderer>().sprite = TofuShape[cut];
            Tcollider = GetComponent<BoxCollider2D>();
            Instantiate(TofuPiece[cut - 1], new Vector3(myTF.x + 3, myTF.y, myTF.z), Quaternion.identity);
            Tcollider.size = new Vector2(3, 2);
        }
        else if ((Input.GetKeyDown(KeyCode.V) || Input.GetKeyDown(KeyCode.C)) && cut != 0 && cut != 3)
        {//かけら切り
            cut = 3;
            GetComponent<SpriteRenderer>().sprite = TofuShape[cut];
            Tcollider = GetComponent<BoxCollider2D>();
            Instantiate(TofuPiece[cut - 1], new Vector3(myTF.x + 3, myTF.y, myTF.z), Quaternion.identity);
            Tcollider.size = new Vector2((float)1.5, (float)1.9);
        }

    }

    //豆取得
    void GetBeans()
    {
        if (Val.beanscheck == true && cut != 0)
        {
            Debug.Log("bean");
            GetComponent<SpriteRenderer>().sprite = TofuShape[0];
            Tcollider.size = new Vector2((float)3.2, (float)3.3);
            cut = 0;
            Val.beanscheck = false;
        }
        else if (Val.beanscheck == true)
        {
            Val.beanscheck = false;
        }

    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "UDfloor")
        {
            transform.SetParent(col.transform);
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "UDfloor")
        {
            transform.SetParent(null);
        }
    }
}