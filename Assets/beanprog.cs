﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beanprog : MonoBehaviour {

    variables Val;

	// Use this for initialization
	void Start () {
        Val = GameObject.Find("Main Camera").GetComponent<variables>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            Val.beanscheck = true;
            Destroy(this.gameObject);
        }
    }

}
