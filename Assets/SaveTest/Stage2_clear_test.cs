﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Stage2_clear_test : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.P))    //Pを押すとステージ２をクリアしたことになる
        {
            PlayerPrefs.SetInt("CLEARSTAGE", 2);
            picbutton.clearst = PlayerPrefs.GetInt("CLEARSTAGE", 2);

            PlayerPrefs.Save();
            SceneManager.LoadScene("Stage3");
        }
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            SceneManager.LoadScene("menu");
        }
    }
   
}
