﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour {

    public int cnt;
    
    public bool nomalswitch;//普通のスイッチの時はこっちを押す
    public bool rideswitch;//押しっぱなしで作動するスイッチはこっちを押す
    [System.NonSerialized]
    public bool[] swiNum;


	// Use this for initialization
	void Start () {

    }

    void OnTriggerEnter2D(Collider2D Player)
    {
        if (nomalswitch && rideswitch==false)
        {
            swiNum[cnt]=true;
            //Debug.Log("スイッチ" + swiNum);
        }
    }


    void OnTriggerStay2D(Collider2D Player)
    {
        if (rideswitch && nomalswitch==false)
        {
            swiNum[cnt]=true;
            Debug.Log("スイッチ" + swiNum);
        }
    }

    // Update is called once per frame
    void Update () {
        
	}
}
